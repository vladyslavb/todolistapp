//
//  MainDataDisplayManager.m
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainDataDisplayManager.h"

//classes
#import "Task+CoreDataClass.h"
#import "TaskCell.h"
#import "MainTableViewController.h"
#import "DataManager.h"

#import <MagicalRecord/MagicalRecord.h>

//static variables
static NSString* kTaskIdentifier = @"TaskCell_ID";

@interface MainDataDisplayManager()

//properties
@property (weak, nonatomic) id<DataDisplayManagerOutput> output;

@property (strong, nonatomic) DataManager* dataManager;

@property (nonatomic) NSMutableArray* tasks;

@end


@implementation MainDataDisplayManager

#pragma mark - Initialization -

- (instancetype) initWithOutput: (id<DataDisplayManagerOutput>) output
{
    if (self == [super init])
    {
        self.output = output;
        
        self.dataManager = [DataManager sharedManager];
    }
    
    return self;
}

- (NSMutableArray*) tasks
{
    if (_tasks == nil)
    {
        _tasks = [self.output getAllTasks];
    }

    return _tasks;
}


#pragma mark - UITableViewDataSource methods -

- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
    return 1;
}

- (NSInteger) tableView: (UITableView*) tableView
  numberOfRowsInSection: (NSInteger)    section
{
    
    return self.tasks.count;
}

- (UITableViewCell*) tableView: (UITableView*) tableView
         cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    TaskCell* cell = [tableView dequeueReusableCellWithIdentifier: kTaskIdentifier
                                                     forIndexPath: indexPath];

    Task* task = self.tasks[indexPath.row];

    [cell fillContentWithTask: task];
    
    return cell;
}

- (BOOL)            tableView: (UITableView*) tableView
        canMoveRowAtIndexPath: (NSIndexPath*) indexPath
{
    return YES;
}

- (void)        tableView: (UITableView*) tableView
       moveRowAtIndexPath: (NSIndexPath*) sourceIndexPath
              toIndexPath: (NSIndexPath*) destinationIndexPath
{
    if (sourceIndexPath.section == destinationIndexPath.section)
    {
        Task* movingTask = self.tasks[sourceIndexPath.row];
        
        [self.tasks removeObjectAtIndex: sourceIndexPath.row];
        
        [self.tasks insertObject: movingTask
                         atIndex: destinationIndexPath.row];
        
        NSInteger start = sourceIndexPath.row;
        
        if (destinationIndexPath.row < start)
        {
            start = destinationIndexPath.row;
        }
        
        NSInteger end = destinationIndexPath.row;
        
        if (sourceIndexPath.row > end)
        {
            end = sourceIndexPath.row;
        }
        
        NSMutableArray<Task*>* reorderedTasks = [NSMutableArray new];
        
        for (NSInteger i = start; i <= end; i++)
        {
            reorderedTasks[i] = self.tasks[i];
            
            reorderedTasks[i].displayOrder = [NSNumber numberWithInteger: i];
        }
    }
}

- (void) tableView: (UITableView*)                tableView
commitEditingStyle: (UITableViewCellEditingStyle) editingStyle
 forRowAtIndexPath: (NSIndexPath*)                indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        Task* deletingTask = self.tasks[indexPath.row];
        
        [deletingTask MR_deleteEntity];
        
        [self.dataManager saveContext];
        
        [self.tasks removeObjectAtIndex: indexPath.row];
        
        [tableView deleteRowsAtIndexPaths: @[indexPath]
                         withRowAnimation: UITableViewRowAnimationFade];
    }
}


#pragma mark - UITableViewDelegate methods -

- (BOOL)                     tableView: (UITableView*) tableView
shouldIndentWhileEditingRowAtIndexPath: (NSIndexPath*) indexPath
{
    return NO;
}

- (void)              tableView: (UITableView*) tableView
        didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    if ([self.output respondsToSelector: @selector(didSelectItemWithTask:)])
    {
        Task* selectedTask = self.tasks[indexPath.row];
        
        [self.output didSelectItemWithTask: selectedTask];
    }
    
    [tableView deselectRowAtIndexPath: indexPath
                             animated: YES];
}

- (BOOL)               tableView: (UITableView*) tableView
 shouldShowMenuForRowAtIndexPath: (NSIndexPath*) indexPath
{
    return YES;
}


#pragma mark - Public methods -

- (void) fillWithTasks: (NSMutableArray*) tasks
{
    self.tasks = tasks;
}

@end





















