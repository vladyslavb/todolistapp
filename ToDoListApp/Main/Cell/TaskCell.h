//
//  TaskCell.h
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Task;

@interface TaskCell : UITableViewCell

/**
 @author Vladyslav Bedro
 
 Method fill cell with person information
 */
- (void) fillContentWithTask: (Task*) task;

@end
