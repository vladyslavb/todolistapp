//
//  TaskCell.m
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "TaskCell.h"

#import "Task+CoreDataClass.h"

@interface TaskCell ()

//outlets
@property (weak, nonatomic) IBOutlet UIImageView* taskImage;
@property (weak, nonatomic) IBOutlet UILabel*     textTaskLabel;
@property (weak, nonatomic) IBOutlet UILabel*     dateTaskLabel;


@end

@implementation TaskCell


#pragma mark - Default cell methods -

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self configureImageView];
}

- (void) setSelected: (BOOL) selected
            animated: (BOOL) animated
{
    [super setSelected: selected
              animated: animated];
}


#pragma mark - Public methods -

- (void) fillContentWithTask: (Task*) task
{
    self.textTaskLabel.text = task.text;
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat: @"dd-MM-yyyy HH:mm"];
    
    NSDate* taskDate = task.date;
    
    NSString* dateString = [formatter stringFromDate: taskDate];
    
    self.dateTaskLabel.text = dateString;
}


#pragma mark - Private methods -

- (void) configureImageView
{
    self.taskImage.layer.backgroundColor = [[UIColor clearColor] CGColor];
    self.taskImage.layer.cornerRadius    = self.taskImage.frame.size.height / 2;
    self.taskImage.layer.masksToBounds   = YES;
    self.taskImage.layer.borderColor     = [[UIColor whiteColor] CGColor];
    self.taskImage.layer.borderWidth     = 2.0;
    self.taskImage.clipsToBounds         = YES;
}



@end
