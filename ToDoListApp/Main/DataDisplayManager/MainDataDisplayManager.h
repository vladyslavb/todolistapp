//
//  MainDataDisplayManager.h
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import <CoreData/CoreData.h>

//classes
#import "Task+CoreDataClass.h"

//protocols
#import "DataDisplayManagerOutput.h"

@interface MainDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>


//methods

/**
 @author Vladyslav Bedro
 
 Method initializes and binds components of the MVC pattern
 */
- (instancetype) initWithOutput: (id<DataDisplayManagerOutput>) output;

/**
 @author Vladyslav Bedro
 
 Method fills delegate of mainTableViewController with all tasks
 */
- (void) fillWithTasks: (NSMutableArray*) tasks;

@end
