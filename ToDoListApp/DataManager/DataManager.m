//
//  DataManager.m
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "DataManager.h"

//classes
#import <CoreData/CoreData.h>
#import "Task+CoreDataClass.h"

#import <MagicalRecord/MagicalRecord.h>

@implementation DataManager


#pragma mark - Singleton method -

+ (DataManager*) sharedManager
{
    static DataManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DataManager alloc] init];
    });
    
    return manager;
}


#pragma mark - Core Data Saving support

- (void) saveContext
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:
     
        ^(BOOL success, NSError *error) {
            
        if (success)
        {
            NSLog(@"You successfully saved your context.");
        }
        else if (error)
        {
            NSLog(@"Error saving context: %@", error.description);
        }
    }];
}

@end

























