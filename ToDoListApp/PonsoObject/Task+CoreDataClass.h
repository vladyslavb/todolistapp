//
//  Task+CoreDataClass.h
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Task : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Task+CoreDataProperties.h"
