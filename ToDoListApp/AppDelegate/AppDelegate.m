//
//  AppDelegate.m
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AppDelegate.h"

//classes
#import "DataManager.h"
#import <MagicalRecord/MagicalRecord.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


#pragma mark - Default App Delegate methods -

- (BOOL)          application: (UIApplication*) application
didFinishLaunchingWithOptions: (NSDictionary*)  launchOptions
{
    [MagicalRecord setupCoreDataStackWithStoreNamed: @"ToDoListApp"];
    
    return YES;
}


- (void) applicationWillResignActive: (UIApplication*) application
{

}


- (void) applicationDidEnterBackground: (UIApplication*) application
{

}


- (void) applicationWillEnterForeground: (UIApplication*) application
{

}


- (void) applicationDidBecomeActive: (UIApplication*) application
{

}


- (void) applicationWillTerminate: (UIApplication*) application
{
    [self saveContext];
}


#pragma mark - Core Data internal methods -

- (void) saveContext
{
    DataManager* dataManager = [DataManager sharedManager];
    
    [dataManager saveContext];
}

@end




























