//
//  MainTableViewController.m
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainTableViewController.h"

//classes

#import "Task+CoreDataClass.h"
#import "DataManager.h"
#import <MagicalRecord/MagicalRecord.h>

// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface MainTableViewController ()

//outlets
@property (weak, nonatomic) IBOutlet UITableView*     mainTableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* editModeButton;

//properties
@property (strong, nonatomic) MainDataDisplayManager* displayManager;

@property (strong, nonatomic) DataManager* dataManager;

@property (nonatomic) NSMutableArray* tasks;

@end

@implementation MainTableViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self bindingUI];
}

- (void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear: animated];

    [self fetchAllTasks];
    [self.mainTableView reloadData];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Properties -

- (MainDataDisplayManager*) displayManager
{
    if (_displayManager == nil)
    {
        _displayManager = [[MainDataDisplayManager alloc] initWithOutput: self];
    }
    
    return _displayManager;
}


#pragma mark - Display manager output methods -

- (void) didSelectItemWithTask: (Task*) task
{
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat: @"dd-MM-yyyy HH:mm"];
    
    NSDate* taskDate = task.date;
    
    NSString* dateString = [formatter stringFromDate: taskDate];
    
    NSString* alertMessage = [NSString stringWithFormat: @"%@: \n %@", dateString, task.text];
    
    [self simpleAlertWithAlert: @"Task Information"
                   withMessage: alertMessage];
}

- (NSMutableArray*) getAllTasks
{
    [self fetchAllTasks];
    
    return self.tasks;
}


#pragma mark - Actions -

- (IBAction) onAddTaskPressed: (UIBarButtonItem*) sender
{
    [self createAlertWithTextFields];
}

- (IBAction) onEditButtonPressed: (UIBarButtonItem*) sender
{
    [self.mainTableView setEditing: !self.mainTableView.editing
                          animated: YES];
    
    if (self.mainTableView.editing)
    {
        self.editModeButton.tintColor = [UIColor blackColor];
    }
    else
    {
        self.editModeButton.tintColor = [UIColor blueColor];
    }
}


#pragma mark - UI -

- (void) bindingUI
{
    self.mainTableView.dataSource                   = self.displayManager;
    self.mainTableView.delegate                     = self.displayManager;
    self.mainTableView.editing                      = NO;
    self.mainTableView.allowsSelectionDuringEditing = NO;
    
    self.dataManager = [DataManager sharedManager];
    
    [self.mainTableView registerNib: [UINib nibWithNibName: @"TaskCell"
                                                    bundle: nil]
             forCellReuseIdentifier: @"TaskCell_ID"];
}


#pragma mark - Internal methods -

- (void) createAlertWithTextFields
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle: @"New Task"
                                                                             message: @"Input title and date of new task"
                                                                      preferredStyle: UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler: ^(UITextField * _Nonnull textField) {
        
        textField.placeholder     = @"Enter title of a task";
        textField.textColor       = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle     = UITextBorderStyleRoundedRect;
    }];
    
    [alertController addTextFieldWithConfigurationHandler: ^(UITextField * _Nonnull textField) {
        
        textField.placeholder     = @"Enter date \"dd-MM-yyyy HH:mm\"";
        textField.textColor       = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle     = UITextBorderStyleRoundedRect;
    }];
    
    UIAlertAction* alertAction = [UIAlertAction actionWithTitle: @"OK"
                                                          style: UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction *action) {
                                                            
                                        NSArray* textFields    = alertController.textFields;
                                        UITextField* textField = textFields[0];
                                        UITextField* dateField = textFields[1];
                                                            
                                        NSString* dateString = dateField.text;
                                                            
                                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                                                            
                                        [dateFormatter setDateFormat: @"dd-MM-yyyy HH:mm"];
                                                            
                                        NSDate* date = [dateFormatter dateFromString: dateString];
                                                            
                                        [self insertNewTaskWithText: textField.text
                                                        andWithDate: date];
                                                        }];
    
    [alertController addAction: alertAction];
    
    [self presentViewController: alertController
                       animated: YES
                     completion: nil];
}

- (void) simpleAlertWithAlert: (NSString*) title
                  withMessage: (NSString*) message
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle: title
                                                                             message: message
                                                                      preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction* alertAction = [UIAlertAction actionWithTitle: @"OK"
                                                          style: UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction * _Nonnull action) {
                                                            //
                                                        }];
    
    [alertController addAction: alertAction];
    
    [self presentViewController: alertController
                       animated: YES
                     completion: nil];
}

- (void) insertNewTaskWithText: (NSString*) text
                   andWithDate: (NSDate*)   date
{
    Task* newTask = [Task MR_createEntity];
    
    newTask.text = text;
    newTask.date = date;
    
    newTask.displayOrder = [NSNumber numberWithInteger: self.tasks.count];

    [self.dataManager saveContext];
    
    [self fetchAllTasks];
    [self.displayManager fillWithTasks: self.tasks];
    [self.mainTableView reloadData];
}

- (void) createNotificationWithText: (NSString*) text
                        andWithDate: (NSDate*)   date
{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    center.delegate = self;
    
    [center requestAuthorizationWithOptions: (UNAuthorizationOptionBadge |
                                              UNAuthorizationOptionSound |
                                              UNAuthorizationOptionAlert)
                          completionHandler: ^(BOOL granted, NSError * _Nullable error) {
                              if(error == nil)
                              {
                                  [[UIApplication sharedApplication] registerForRemoteNotifications];
                                  
                                  NSLog( @"Push registration success");
                              }
                              else
                              {
                                  NSLog( @"Push registration FAILED");
                                  
                                  NSLog( @"ERROR: %@ - %@",
                                        error.localizedFailureReason,
                                        error.localizedDescription);
                                  
                                  NSLog( @"SUGGESTIONS: %@ - %@",
                                        error.localizedRecoveryOptions,
                                        error.localizedRecoverySuggestion);
                              }
                          }];
    
    UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
    
    content.title = [NSString localizedUserNotificationStringForKey: @"Your task:"
                                                          arguments: nil];
    
    content.body = [NSString localizedUserNotificationStringForKey: text
                                                         arguments: nil];
    content.sound = [UNNotificationSound defaultSound];
    
    content.badge = [NSNumber numberWithInteger: ([UIApplication sharedApplication].applicationIconBadgeNumber + 1)];
    
    NSDate *referenceDate = [NSDate date];
    
    NSTimeInterval timeInterval = [date timeIntervalSinceDate: referenceDate];
    
    UNTimeIntervalNotificationTrigger* trigger =
        [UNTimeIntervalNotificationTrigger triggerWithTimeInterval: timeInterval
                                                           repeats: NO];
    
    UNNotificationRequest* request = [UNNotificationRequest requestWithIdentifier: @"ToDoListApp"
                                                                          content: content
                                                                          trigger: trigger];
    
    [center addNotificationRequest: request
             withCompletionHandler: ^(NSError * _Nullable error) {
                 
                 if (error)
                 {
                    NSLog(@"add NotificationRequest error!");
                 }
    }];
}

- (void) fetchAllTasks
{
    self.tasks = [[Task MR_findAllSortedBy: @"displayOrder"
                                 ascending: YES]
                                            mutableCopy];
    
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey: @"displayOrder"
                                                                   ascending: YES];
    
    NSArray* sortDescriptors = [[NSArray alloc] initWithObjects: &sortDescriptor
                                                          count: 1];
    
    [self.tasks sortUsingDescriptors: sortDescriptors];
}



@end





























