//
//  DataDisplayManagerOutput.h
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Task;

@protocol DataDisplayManagerOutput <NSObject>

//methods

/**
 @author Vladyslav Bedro
 
 Method make push and moving to information controller with task information
 */
- (void) didSelectItemWithTask: (Task*) task;

/**
 @author Vladyslav Bedro
 
 Method gets all of tasks
 */
- (NSMutableArray*) getAllTasks;

@end
