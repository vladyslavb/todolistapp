//
//  Task+CoreDataProperties.h
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//
//

#import "Task+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Task (CoreDataProperties)

+ (NSFetchRequest <Task*>*)fetchRequest;

//properties

@property (nullable, nonatomic, copy) NSString* text;
@property (nullable, nonatomic, copy) NSDate*   date;
@property (strong,         nonatomic) NSNumber* displayOrder;

@end

NS_ASSUME_NONNULL_END
