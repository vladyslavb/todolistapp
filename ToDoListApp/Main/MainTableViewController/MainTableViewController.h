//
//  MainTableViewController.h
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainDataDisplayManager.h"
#import <UserNotifications/UserNotifications.h>

//protocols
#import "DataDisplayManagerOutput.h"

@interface MainTableViewController : UIViewController <DataDisplayManagerOutput, UNUserNotificationCenterDelegate>

//properties
@property (weak, nonatomic) id<DataDisplayManagerOutput> output;


@end
