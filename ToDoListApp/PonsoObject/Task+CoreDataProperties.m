//
//  Task+CoreDataProperties.m
//  ToDoListApp
//
//  Created by Vladyslav Bedro on 8/13/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//
//

#import "Task+CoreDataProperties.h"

@implementation Task (CoreDataProperties)

+ (NSFetchRequest <Task*>*) fetchRequest
{
	return [NSFetchRequest fetchRequestWithEntityName: @"Task"];
}

//properties

@dynamic text;
@dynamic date;
@dynamic displayOrder;

@end
